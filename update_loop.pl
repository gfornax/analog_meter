#!/usr/bin/perl

use strict;
use warnings;
use POSIX qw(strftime);
use Time::HiRes qw(gettimeofday usleep);
use Device::SerialPort; # qw( :PARAM :STAT 0.07 );

# no idea why, but does not work otherwise
open(my $comfh, "<", "/dev/ttyUSB1") or die "cannot open: $!";

my $port = Device::SerialPort->new("/dev/ttyUSB1") or die "$!"; 
$port->baudrate(38400); 
$port->parity("none"); 
$port->databits(8); 
$port->stopbits(1); 
$port->handshake("none") || die "failed setting handshake: $!";
$port->baudrate(38400); 
$port->baudrate(19200); 
$port->write_settings or croak("Failed setting... everything: $!");
#$port->read_char_time(0);     # don't wait for each character
#$port->read_const_time(1000); # 1 second per unfulfilled "read" call
#$port->lookclear; 
$port->write("\nAT+145255142255\r\n");
#$port->lookclear; 

#while(<$comfh>){
#	print $_;
#}
print "init comfh done\n";

my $dev = @ARGV ? shift : 'eth0';
my $rxbytes = "/sys/class/net/$dev/statistics/rx_bytes";
my $txbytes = "/sys/class/net/$dev/statistics/tx_bytes";
my $cpustats = "/proc/stat";


open(my $rxfh, "<", "$rxbytes")  or die "cannot open: $!";
open(my $txfh, "<", "$txbytes")  or die "cannot open: $!";
open(my $cpufh, "<", "$cpustats")  or die "cannot open: $!";

my $rxlast=0;
my $txlast=0;
my $cpuloadlast=0;
my $cpuidlelast=0;

while ( 1 ){
	my $rxinput;
	my $txinput;
	my $cpuloadinput;
	my $cpuidleinput;
	sysread $rxfh,$rxinput,20,0;
	sysread $txfh,$txinput,20,0;
	chomp $rxinput;
	chomp $txinput;
	my $cpuline = <$cpufh>;
	chomp $cpuline;
	my @split = split / +/, $cpuline;
	$cpuloadinput = $split[1] + $split[2] + $split[3];
	$cpuidleinput = $split[4];

	my $currx = $rxinput - $rxlast;
	my $curtx = $rxinput - $rxlast;
	my $curcpu = 1.0*($cpuloadinput-$cpuloadlast)/($cpuloadinput-$cpuloadlast+$cpuidleinput-$cpuidlelast);

	print "rx current: $currx\n";
	my $rxmbit = int((($currx*8.0*10)/(1024*1024)) * 1.45 * 4);
	my $cpuex = int($curcpu * 145 ); # last fact: numcpus
	my $txstring = sprintf("AT+%.3d255%.3d255\r\n", $rxmbit, $cpuex);

	print "rx: $rxmbit cpu: $cpuex \n" if $cpuidlelast != 0;
	print $txstring . "\n" if $cpuidlelast != 0;
		
	$port->write($txstring);

	$rxlast = $rxinput;
	$txlast = $txinput;
	$cpuloadlast = $cpuloadinput;
	$cpuidlelast = $cpuidleinput;


	sysseek $rxfh, 0, 0;
	sysseek $txfh, 0, 0;
	seek $cpufh, 0, 0;
	usleep(250000);

#while(<$comfh>){
#	print $_;
#}
}
close $rxfh;
close $txfh;
close $cpufh;
