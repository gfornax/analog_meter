# Makefile to build light_ws2812 library examples
# This is not a very good example of a makefile - the dependencies do not work, therefore everything is rebuilt every time.

# Change these parameters for your device

F_CPU = 16000000
DEVICE = atmega328p

# Tools:
CC = avr-gcc

LIB     = 
SRC  	= main
DEP	=  uart.h

CFLAGS = -g2 -I. -mmcu=$(DEVICE) -DF_CPU=$(F_CPU) 
CFLAGS+= -Os -ffunction-sections -fdata-sections -fpack-struct -fno-move-loop-invariants -fno-tree-scev-cprop -fno-inline-small-functions  
CFLAGS+= -Wall -Wno-pointer-to-int-cast -std=gnu99
#CFLAGS+= -Wa,-ahls=$<.lst

LDFLAGS = -Wl,--relax,--section-start=.text=0,-Map=main.map

AVRDUDE=avrdude
AVRDUDE_PROGRAMMER = avrispmkii

# com1 = serial port. Use lpt1 to connect to parallel port.
AVRDUDE_PORT = usb    # programmer connected to serial device
AVRDUDE_WRITE_FLASH = -U flash:w:$(SRC).hex
AVRDUDE_FLAGS = -p $(DEVICE) -P $(AVRDUDE_PORT) -B 2 -c $(AVRDUDE_PROGRAMMER)



all:	$(SRC)

$(SRC): $(LIB) 
	@echo Building $@
	@$(CC) $(CFLAGS) -o Objects/$@.o $@.c uart.c
	@avr-size Objects/$@.o
	@avr-objcopy -j .text  -j .data -O ihex Objects/$@.o $@.hex
	@avr-objdump -d -S Objects/$@.o >Objects/$@.lss

program: $(SRC)
	@avrdude $(AVRDUDE_FLAGS) $(AVRDUDE_WRITE_FLASH)

.PHONY:	clean

clean:
	rm -f *.hex Objects/*.o Objects/*.lss
