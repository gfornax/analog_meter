#include <stdlib.h>
#include <avr/io.h>
#include <stdio.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include "uart.h"


void init_timer( void );
void init_io( void );
//enum states { WAIT_PREFIX, READ_CMD, } mstate;
int usart_putchar_printf(char var, FILE *stream);

static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar_printf, NULL, _FDEV_SETUP_WRITE);

void init( void )
{
	InitUART( 50 );
	stdout = &mystdout;
	sei();
}

void init_io(void)
{
	DDRD |= (1 << PD5) | (1 << PD6);
	DDRB |= (1 << PB1) | (1 << PB2);
}

void init_timer(void)
{
	TCCR0A |= (1 << COM0A1) | (1 << COM0B1) | (1 << WGM00);
	TCCR0B |= (1 << CS01) | (1 << CS00);
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1) | (1 << WGM10);
	TCCR1B |= (1 << CS11) | (1 << CS10);
}

uint8_t tchar2int(const char *s)
{
	uint16_t result = 0;
	for(uint8_t i = 0; i < 3; i++){
		if(s[i] < 0x30 || s[i] > 0x39)
			return 0;
	}
	result = (s[0]-0x30) * 100;
	result += (s[1]-0x30) * 10;
	result += (s[2]-0x30);
	if (result > 255)
		return 0;
	else
		return (uint8_t)result;
}

int main(void)
{
	CLKPR=_BV(CLKPCE);
	CLKPR=0;
	char cmdbuf[17];
	uint8_t prebuf_cnt = 0;
	uint8_t cmdbuf_cnt = 0;

	_delay_ms(50);
	init_io();
	init_timer();
	init();


	printf("init done, accepting commands\n");
	while ( 1 ){
		char recbyte = (char)ReceiveByte();
		if (recbyte == '\n')
			continue;
		cmdbuf[cmdbuf_cnt++] = recbyte;
		if (cmdbuf_cnt > 16){
			memset(cmdbuf, 0, cmdbuf_cnt);
			cmdbuf_cnt = 0;
			printf("invalid cmd\n");
		} else if (cmdbuf[cmdbuf_cnt-1] == '\r'){
			if (cmdbuf_cnt == 16){
				/* valid cmd */
				if (strncmp("AT+", cmdbuf, 3) == 0){
					uint8_t mbit_set = tchar2int(&cmdbuf[3]); 
					uint8_t mbit_bg = tchar2int(&cmdbuf[6]); 
					uint8_t load_set = tchar2int(&cmdbuf[9]); 
					uint8_t load_bg = tchar2int(&cmdbuf[12]);
					OCR0A = load_set;
					OCR1A = mbit_bg;
					OCR0B = mbit_set;
					OCR1B = load_bg;
					printf("setting values to Mbit/BG Load/BG %d/%d %d/%d\n", 
					mbit_set, mbit_bg, load_set, load_bg);	
				}
			} 
			/* invalid cmd or successfully interpreted  */
			memset(cmdbuf, 0, cmdbuf_cnt);
			cmdbuf_cnt = 0;
		}
	}
}

int usart_putchar_printf(char var, FILE *stream)
{
    if (var == '\n')  TransmitByte('\r');
    TransmitByte(var);
    return 0;
}


