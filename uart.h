#ifndef _UART_H_
#define _UART_H_
/* Prototypes */
void InitUART( unsigned char baudrate );
unsigned char ReceiveByte( void );
void TransmitByte( unsigned char data );
unsigned char DataInReceiveBuffer( void );
#endif /* UART_H_*/
